import java.util.Scanner;
public class Aufgabe3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int zahl;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie eine Zahl ein");
		zahl = sc.nextInt();
		
		if(zahl >= 0)
			System.out.println("Die Zahl ist positiv!");
		
		else
			System.out.println("Die Zahl ist negativ");
		
		sc.close();
	}

}
