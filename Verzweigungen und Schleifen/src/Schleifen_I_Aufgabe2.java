import java.util.Scanner;

public class Schleifen_I_Aufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int divisor;
		int divident = 75;
		Scanner sc = new Scanner(System.in);
		System.out.println("Die Zahlen von 75 bis 150 werden durch eine von Ihnen gew�hlte Zahl geteilt. \nGeben Sie eine Zahl ein.");
		divisor = sc.nextInt();
		
		while (divident <=150) {
			if(divident % divisor == 0) {
				System.out.println(divident + " ist durch " + divisor + " teilbar");
			}
			else {
				System.out.println(divident + " ist nicht durch " + divisor + " teilbar");
			}
			divident++;
		}
		sc.close();
	}

}
