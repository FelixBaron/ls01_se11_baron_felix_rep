import java.util.Scanner;

public class Schleifen_I_Aufgabe5 {

	public static void main(String[] args) {
	
	Scanner sc = new Scanner(System.in);
	int eingabe;
	int zaehler = 0;
	
	System.out.println("Bitte geben Sie eine Zahl ein.");
	eingabe = sc.nextInt();
	
	while(eingabe%5 == 0) {
		eingabe = eingabe/5;
		//System.out.println("eingabe" + eingabe);
		zaehler ++;
		//System.out.println("zaehler" + zaehler);
	}
	
	System.out.println("Ihre Zahl ist " + zaehler + " Mal durch 5 teilbar.");
	sc.close();
	}
}
