import java.util.Scanner;

public class Schleifen_I_Aufgabe4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int eingabe;
		int summe = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine Zahl ein.");
		eingabe = sc.nextInt();
		System.out.print(eingabe + " -> ");
		for(int summand = 1; summand <= eingabe; summand++) {
			summe += summand;
			
			if(summand == eingabe) {
				System.out.print(summand + " = " + summe);
			}
			else {
				System.out.print(summand + " + ");
			}
			
		}
		
		
		
		sc.close();
	}

}
