import java.util.Scanner;

public class Schleifen_I_Aufgabe3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int divident;
		
		System.out.println("Bitte geben Sie eine Zahl ein, deren Teiler Sie wissen m�chten.");
		divident = sc.nextInt();
		
		for(int divisor = divident; divisor >0; divisor--) {
			if(divident % divisor == 0) {
				System.out.println(divident + " ist durch " + divisor + " teilbar.");
			}

		}

				
	sc.close();
	}

}
