import java.util.Scanner;
public class Aufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		long ersteZahl, zweiteZahl;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie eine Zahl ein");
		ersteZahl = sc.nextLong();
		System.out.println("Bitte geben Sie eine weitere Zahl ein");
		zweiteZahl = sc.nextLong();
		
		if(ersteZahl == zweiteZahl)
			System.out.println(ersteZahl + " " + zweiteZahl);
			
		else if(ersteZahl < zweiteZahl)
		{
			System.out.println("\n" + ersteZahl);
			System.out.println(zweiteZahl);
		}
		
		else
		{
			System.out.println("\n" + zweiteZahl);
			System.out.println(ersteZahl);
		}
		
	}

}
