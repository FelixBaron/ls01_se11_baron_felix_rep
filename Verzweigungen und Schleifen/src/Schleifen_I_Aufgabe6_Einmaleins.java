
public class Schleifen_I_Aufgabe6_Einmaleins {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int multiplikator = 1;
		int multiplikand = 1;
		int produkt;
		
		for (multiplikator = 1; multiplikator <= 10; multiplikator++) {
			
			for (multiplikand = 1; multiplikand <= 10; multiplikand++) {
				
				produkt = multiplikator * multiplikand;
				System.out.println(multiplikator + " * " + multiplikand + " = " + produkt);
			}
			
			System.out.println();
		}
			
	}

}
