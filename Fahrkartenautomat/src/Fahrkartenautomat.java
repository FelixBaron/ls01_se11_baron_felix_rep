﻿import java.util.Scanner;
import java.io.*;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		byte programmWiederholung = 0;

		do {

			double ticketpreis = 0.00;
			double zwischensumme = 0.00;
			double zuZahlenderBetrag = 0.00;
			double eingezahlterGesamtbetrag = 0.00;
			float eingeworfeneMünze = 0.00f;
			double rueckgabebetrag = 0.00;
			double rueckgabebetragr = 0.00;
			double preisA = 2.90;
			double preisB = 8.60;
			double preisC = 23.50;

			byte anzahlTickets = 0;
			byte tickettyp = 0;
			byte richtigeEingabe = 1;
			String weitereAusfuehrung = "a";
			String a = "Einzelfahrschein Regeltarif AB";
			String b = "Tageskarte Regeltarif AB";
			String c = "Kleingruppen-Tageskarte Regeltarif AB";

			// Fahrkartenbestellung
			zuZahlenderBetrag = fahrkartenbestellungErfassen(ticketpreis, zwischensumme, zuZahlenderBetrag,
					anzahlTickets, preisA, preisB, preisC, tastatur, a, b, c, tickettyp);

			// Geldeinwurf
			eingezahlterGesamtbetrag = fahrkartenBezahlen(eingezahlterGesamtbetrag, zuZahlenderBetrag,
					eingeworfeneMünze, tastatur);

			// Fahrscheinausgabe
			fahrkartenAusgeben();

			// Rückgeldberechnung und -Ausgabe
			// -------------------------------
			rueckgeldAusgeben(rueckgabebetrag, eingezahlterGesamtbetrag, zuZahlenderBetrag, rueckgabebetragr);
			programmWiederholung = erneuteAusfuehrung(programmWiederholung, tastatur, weitereAusfuehrung, richtigeEingabe);
		} while (programmWiederholung == 1);

		tastatur.close();
	}

	public static double fahrkartenbestellungErfassen(double ticketpreis, double zwischensumme, double zuZahlenderBetrag,
			byte anzahlTickets, double preisA, double preisB, double preisC, Scanner tastatur, String a, String b,
			String c, byte tickettyp) {
		do {

			do {
				System.out.println("Wählen Sie Ihre Wunschfahrkarte für Berlin AB aus:");
				System.out.printf(a + " [ %.2f  EUR] (1)\n", preisA);
				System.out.printf(b + " [ %.2f  EUR] (2)\n", preisB);
				System.out.printf(c + " [ %.2f  EUR] (3)\n", preisC);
				System.out.println("Bezahlen(9)");

				tickettyp = tastatur.nextByte();
				if (tickettyp == 1) {
					ticketpreis = preisA;
					System.out.println("Ihre Wahl: " + a);
				} else if (tickettyp == 2) {
					ticketpreis = preisB;
					System.out.println("Ihre Wahl: " + b);
				} else if (tickettyp == 3) {
					ticketpreis = preisC;
					System.out.println("Ihre Wahl: " + c);
				} else if (tickettyp == 9 && zwischensumme == 0) {
					System.out.println("Sie müssen ein Ticket auswählen. Bitte wählen Sie Ihr Ticket aus.\n");
				} else if (tickettyp == 9) {
					zuZahlenderBetrag = zwischensumme;
					System.out.println();
				} else if (tickettyp < 1 || tickettyp > 3) {
					System.out.println("Falsche Eingabe. Wählen Sie Ihr Ticket erneut aus.\n");
				}
			} while (tickettyp < 1 || tickettyp > 3 && tickettyp != 9 || tickettyp == 9 && zwischensumme == 0);

			if (tickettyp != 9) {
				do {
					System.out.println("\nAnzahl der Tickets (zwischen 1 und 10): ");
					anzahlTickets = tastatur.nextByte();
					if (anzahlTickets < 1 || anzahlTickets > 10) {
						System.out.println(">> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
					}
				}

				while (anzahlTickets < 1 || anzahlTickets > 10);

				zwischensumme += ticketpreis * anzahlTickets;
				System.out.printf("Zwischensumme: %.2f EUR \n\n", zwischensumme);

			}
		}

		while (tickettyp != 9);

		return zuZahlenderBetrag;

	}

	public static double fahrkartenBezahlen(double eingezahlterGesamtbetrag, double zuZahlenderBetrag,
			float eingeworfeneMünze, Scanner tastatur) {
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextFloat();
			eingezahlterGesamtbetrag += eingeworfeneMünze;

		}
		return eingezahlterGesamtbetrag;

	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double rueckgabebetrag, double eingezahlterGesamtbetrag, double zuZahlenderBetrag,
			double rueckgabebetragr) {
		rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		rueckgabebetragr = rueckgabebetrag;
		if (rueckgabebetragr > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von " + "%.2f" + " EURO", rueckgabebetragr);
			System.out.println(" wird in folgenden Münzen ausgezahlt:");

			while (rueckgabebetragr >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rueckgabebetragr -= 2.0;
			}
			while (rueckgabebetragr >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rueckgabebetragr -= 1.0;
			}
			while (rueckgabebetragr >= 0.499) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rueckgabebetragr -= 0.50;
			}
			while (rueckgabebetragr >= 0.199) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rueckgabebetragr -= 0.20;
			}
			while (rueckgabebetragr >= 0.099) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rueckgabebetragr -= 0.10;
			}
			while (rueckgabebetragr >= 0.0499)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rueckgabebetragr -= 0.05;
			}
		}

	}

	public static byte erneuteAusfuehrung(byte programmWiederholung, Scanner tastatur, String weitereAusfuehrung, byte richtigeEingabe) {
		
		do {
			
		System.out.printf("\n\nWollen Sie weitere Fahrkarten kaufen? ja/nein \n");
		weitereAusfuehrung = tastatur.next();
		weitereAusfuehrung = weitereAusfuehrung.toUpperCase();
		
		switch(weitereAusfuehrung) {
		case "JA":
			programmWiederholung = 1;
			richtigeEingabe = 1;
			break;
		case "NEIN":
			programmWiederholung = 0;
			richtigeEingabe = 1;
			break;
		default:
			System.out.println("Falsche Eingabe");
			richtigeEingabe = 0;
			break;
		}
		System.out.println("");
	
		}
		while(richtigeEingabe == 0);
		//programmWiederholung = tastatur.nextByte();

		if (programmWiederholung == 0) {
			System.out.println("Der Fahrkartenkauf wurde beendet.");
			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.");
		}

		if (programmWiederholung != 1 && programmWiederholung != 0) {
			System.out.println("Sie haben keine gültige Eingabe gemacht. Der Fahrkartenkauf wird beendet.");
			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.");
			programmWiederholung = 0;
		}
		return programmWiederholung;

	}
}