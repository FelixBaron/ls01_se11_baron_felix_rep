import java.util.Scanner;

public class Konsoleneingabe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		
		int zahl1, zahl2, ergebnis;

		System.out.println("Bitte geben Sie eine ganze Zahl ein");
		
		zahl1 = sc.nextInt();
		
		System.out.println("Bitte geben Sie eine weitere ganze Zahl ein");
		
		zahl2 = sc.nextInt();
		
		ergebnis = zahl1 + zahl2;
		
		System.out.println("Das Ergebnis ist : "+  zahl1 + " + "+ zahl2 +  " = " + ergebnis);
	
		
		//Aufgabe 2
		
		String name;
		byte alter;
		
		
		System.out.println("\nHerzlich Willkommen! Bitte geben Sie Ihren Namen ein und bestštigen die Eingabe mit Enter");
		
		name = sc.next();
		
		System.out.println("Bitte geben Sie Ihr Alter ein");
		
		alter = sc.nextByte();
		
		System.out.println("Ihr Name lautet: " + name + ". Sie sind " + alter + " Jahre alt. ");
		
		sc.close();
		
		
	}

}
