
public class Konsolenausgabe�bung2 {

	public static void main(String[] args) {

		//Aufgabe1
		System.out.println("Aufgabe1");
		String First = "**";
		System.out.printf("%5s\n",First);
		System.out.printf("%-7.1s%.1s\n",First,First);
		System.out.printf("%-7.1s%.1s\n",First,First);
		System.out.printf("%5s\n\n",First);

		//Aufgabe2
		System.out.println("Aufgabe2\n");
		String a = "= 1 * 2 * 3 * 4 * 5 ";
		String b = "1!";
		String c = "2!";
		String d = "3!";
		String e = "4!";
		String f = "5!";
		String g = "=   1";
		String h = "=   2";
		String i = "=   6";
		String j = "=  24";
		String k = "= 120";
		
		System.out.printf("%-5s%-20.3s%s\n",b,a,g);
		System.out.printf("%-5s%-20.7s%s\n",c,a,h);
		System.out.printf("%-5s%-20.11s%s\n",d,a,i);
		System.out.printf("%-5s%-20.15s%s\n",e,a,j);
		System.out.printf("%-5s%-20s%s\n\n",f,a,k);
		
		//Aufgabe3
		System.out.println("Aufgabe3");
		int l = -20;
		int m = -10;
		String n = "+0";
		int o = +20;
		int p = +30;
		double q = -28.8889;
		double r = -23.3333;
		double s = -17.7778;
		double t = -6.6667;
		double u = -1.1111;
		String v = "Fahrenheit";
		String w = "Celcius";
		System.out.printf("%-12s|%10s\n",v,w);
		System.out.println("-----------------------");
		System.out.printf("%-12d|%10f\n",l,q);
		System.out.printf("%-12d|%10f\n",m,r);
		System.out.printf("%-12s|%10f\n",n,s);
		System.out.printf("%-12d|%10f\n",o,t);
		System.out.printf("%-12d|%10f\n",p,u);
	}

}
