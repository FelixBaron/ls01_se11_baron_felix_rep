import java.util.Scanner;

class Addition {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {		//Methodenkopf
    	
    	//public - Zugriffsmodifizierer
    	//void - kein R�ckgabedatentyp
    	//main - Methodenname

        double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;

        programmhinweis();							//methodenaufruf

        zahl1=eingabe(zahl1, "1. Zahl");			//methodenaufruf
        zahl2=eingabe(zahl2, "2. Zahl");			//methodenaufruf
        
        erg = verarbeitung(erg, zahl1, zahl2);		//methodenaufruf mit drei Argumenten
        
        ausgabe(erg, zahl1, zahl2);					//methodenaufruf
        
        
    } // end of main
    												// Zeile 7 bis 25 Methodendefinition; Zeile 8 bis 24 Methodenrumpf
    
    public static void programmhinweis() {			//methodenbezeichner
    	System.out.println("Hinweis: ");
    	System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");
    } // end of programmhinweis
    
    public static double eingabe(double m_zahl1, String text) {
    	 System.out.println(text);
         m_zahl1 = sc.nextDouble();
         return(m_zahl1);							//r�ckgabewert
    } // end of eingabe
    					lokale Variable					//Parameter
    public static double verarbeitung(double erg, double v_zahl1, double v_zahl2) {
    	
    	erg = v_zahl1 + v_zahl2;
    	return(erg);								//r�ckgabewert
    } // end of verarbeitung
    
    
    public static void ausgabe(double erg, double zahl1, double zahl2) {
        System.out.println("Ergebnis der Addition");
        System.out.printf("%.2f = %.2f+%.2f", erg, zahl1, zahl2);
    } // end of ausgabe
    
    	
}