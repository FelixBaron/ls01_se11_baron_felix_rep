import java.util.Scanner;
public class Aufgabe02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		byte monat;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("In welchem Monat (1-12) sind Sie geboren?");
		monat = sc.nextByte();
		
		
		if(monat < 1 || monat > 12)
			System.out.println("Ung�ltige Eingabe");
		
		else if(monat <= 3)
			System.out.println("Sie sind im ersten Quartal geboren");
		
		else if(monat <= 6)
			System.out.println("Sie sind im zweiten Quartal geboren");
		
		else if(monat <= 9)
			System.out.println("Sie sind im dritten Quartal geboren");
		
		else if(monat <= 12)
			System.out.println("Sie sind im vierten Quartal geboren");

	}

}
