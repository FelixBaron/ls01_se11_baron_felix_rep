import java.util.Scanner;
public class Aufgabe03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int stueckzahl;
		float preis1 = 1.1f;
		float preis2 = 0.95f;
		float preis3 = 0.85f;
		float gesamtpreis = 0.00f;
		float bezugspreis = 0.00f;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie ein, wie viele DVDs Sie kaufen m�chten");
		stueckzahl = sc.nextInt();
		
		if(stueckzahl < 1)
			System.out.println("Ung�ltige Eingabe");
		
		else if(stueckzahl <= 9)
		{
			gesamtpreis = berechnung1(preis1, gesamtpreis, stueckzahl);
			bezugspreis = gesamtpreis + 15;
			System.out.printf("Sie m�ssen insgesamt " + "%.2f" + " � bezahlen (inkl. 15 � Versandkosten).", bezugspreis);
		}
		
		else if(stueckzahl <= 49)
		{
			gesamtpreis = berechnung2(preis2, gesamtpreis, stueckzahl);
			bezugspreis = gesamtpreis + 15;
			System.out.printf("Sie m�ssen insgesamt " + "%.2f" + " � bezahlen (inkl. 15 � Versandkosten).", bezugspreis);
		}
		
		else
		{
			gesamtpreis = berechnung3(preis3, gesamtpreis, stueckzahl);
			if(gesamtpreis < 60)
			{
				bezugspreis = gesamtpreis + 15;
				System.out.printf("Sie m�ssen insgesamt " + "%.2f" + " � bezahlen (inkl. 15 � Versandkosten).", bezugspreis);
			}
			
			else
				bezugspreis = gesamtpreis;
				System.out.printf("Sie m�ssen insgesamt " + "%.2f" + " � bezahlen.", bezugspreis);
		}
		
	}
	
	public static float berechnung1(float preis1, float gesamtpreis, int stueckzahl) {
		gesamtpreis = preis1 * stueckzahl;
		return gesamtpreis;
	}
	
	public static float berechnung2(float preis2, float gesamtpreis, int stueckzahl) {
		gesamtpreis = preis2 * stueckzahl;
		return gesamtpreis;
	}
	
	public static float berechnung3(float preis3, float gesamtpreis, int stueckzahl) {
		gesamtpreis = preis3 * stueckzahl;
		return gesamtpreis;
		
	}

}
